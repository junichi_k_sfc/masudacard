//
//  ViewController.swift
//  masudaCar
//
//  Created by A12653 on 2017/04/10.
//  Copyright © 2017年 A12653. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var d: UITextView!
    
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
        }
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)

        UIDevice.current.isBatteryMonitoringEnabled = true
    }
    
    var charg = ""
    func update() {
        if UIDevice.current.batteryState == UIDeviceBatteryState.charging {
            charg += "充電 "
            charg += "(UIApplication.shared.isIdleTimerDisabled):"
            charg += String(UIApplication.shared.isIdleTimerDisabled)
            print("充電チューー")
            
            UIApplication.shared.isIdleTimerDisabled = true
        }
        
        if UIDevice.current.batteryState == UIDeviceBatteryState.unplugged {
            print("充電抜いてるー")
            
            charg += "充電してない\n"
            charg += "(UIApplication.shared.isIdleTimerDisabled):"
            charg += String(UIApplication.shared.isIdleTimerDisabled)
            UIApplication.shared.isIdleTimerDisabled = false
        }
        
        print(Date(), UIApplication.shared.backgroundTimeRemaining)
        charg += "\n"
        d.text = charg
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

